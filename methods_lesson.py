

def car_check(str):
    return "car" in str.lower()


res = car_check('My Car is good')
print(res)


def pig_latin(word):
    first_ltt = word[0]

    # check if theres a vowel

    if first_ltt in "aeiou":
        pig_word = word + "ai"
    else:
        pig_word = word[1:] + first_ltt + "ay"

    return pig_word


result = pig_latin("algeber")
print(result)
