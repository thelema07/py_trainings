# print words only with specific conditions

st = 'Print only the words that start with s in this sentence'
result = []

sWord = st.split()
for x in sWord:
    if x.find('s') != -1:
        result.append(x)

print(result)

# two version of exercise 2

list1 = [x for x in range(0, 11) if x % 2 == 0]
print(list1)

list2 = []

for x in range(0, 11):
    if x % 2 == 0:
        list2.append(x)

print(list2)

# list comprehension of numbers with condition

listCp = [x for x in range(1, 50) if x % 3 == 0]

print(listCp)

# checking length and displaying even

st = 'Print every word in this sentence that has an even number of letters'

print(len(st))
if(len(st) % 2 == 0):
    print('Even')

even_words = st.split()

for word in even_words:
    if len(word) % 2 == 0:
        print(word)

list_x = [x for x in st.split() if len(x) % 2 == 0]
print(list_x)

# classic fizzbuzz

list_fizz_buzz = []

for x in range(1, 101):
    if x % 5 == 0 and x % 3 == 0:
        list_fizz_buzz.append("FizzBuzz")
    elif x % 3 == 0:
        list_fizz_buzz.append("Fizz")
    elif x % 5 == 0:
        list_fizz_buzz.append("Buzz")
    else:
        list_fizz_buzz.append(x)

print(list_fizz_buzz)
