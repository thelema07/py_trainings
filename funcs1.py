
def myFn():
    print('Hello World')


myFn()


def pr1(name="Danny"):
    return "Hello " + name


def bool(a):
    if a == True:
        return 'Hello'
    elif a == False:
        return 'Goodbay'


def bool2(x, y, z):
    if z == True:
        return x
    else:
        return y


def op1(a, b):
    return a * b


def in_evenly_divisible_by_3(n):
    return n % 3 == 0


def is_greater(c, d):
    return c > d
